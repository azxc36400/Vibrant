package net.cydhra.vibrant.command.commands

import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.module.kotlin.readValue
import com.uchuhimo.konf.NoSuchItemException
import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.configuration.ConfigurationService
import org.cef.callback.CefQueryCallback

object SetCommand : CommandHandler<SetCommandArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: SetCommandArguments, callback: CefQueryCallback?) {
        try {
            ConfigurationService.config[arguments.setting] = ConfigurationService.config.mapper.readValue<Any>(arguments.value)
            callback?.success("successfully updated value")
        } catch (e: NoSuchItemException) {
            callback?.failure(-1, "no such setting")
        } catch (e: JsonParseException) {
            callback?.failure(-2, "could not parse setting")
        }
    }
}

class SetCommandArguments(parser: ArgParser) {
    val setting by parser.positional("SETTING", help = "the name of the requested setting")
    val value by parser.positional("VALUE", help = "the value that shall be stored in the setting")
}