package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

object DebugCommand : CommandHandler<String>(userCommand = true, proxyCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: String, callback: CefQueryCallback?) {
        CommandDispatcher.dispatchCommand(arguments, CommandSender.CLIENT, callback)
    }
}