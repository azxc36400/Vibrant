package net.cydhra.vibrant.command.commands

import com.uchuhimo.konf.NoSuchItemException
import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.configuration.ConfigurationService
import org.cef.callback.CefQueryCallback

/**
 * Get a setting from the config
 */
object GetCommand : CommandHandler<GetCommandArguments>(userCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: GetCommandArguments, callback: CefQueryCallback?) {
        try {
            callback
                    ?.success(ConfigurationService.config.mapper.writeValueAsString(ConfigurationService.config.get<Any>(arguments.setting)))
        } catch (e: NoSuchItemException) {
            callback?.failure(-1, "No such setting")
        }
    }
}

class GetCommandArguments(parser: ArgParser) {
    val setting by parser.positional("SETTING", help = "the name of the requested setting")
}
