package net.cydhra.vibrant.command.commands

import com.google.common.collect.ArrayListMultimap
import com.google.gson.JsonArray
import com.google.gson.JsonElement
import com.google.gson.JsonObject
import com.google.gson.JsonPrimitive
import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import net.cydhra.vibrant.modulesystem.ModuleManager
import org.cef.callback.CefQueryCallback

object ModuleNamesCommand : CommandHandler<ModuleNamesArguments>(userCommand = false) {

    override fun executeCommand(origin: CommandSender, label: String, arguments: ModuleNamesArguments, callback: CefQueryCallback?) {
        val byCategories = ArrayListMultimap.create<String, String>()

        ModuleManager.modules
                .filter { module -> !module.category.hidden }
                .forEach { module ->
                    byCategories.put(module.category.name, module.name)
                }

        val jsonObj = JsonObject()
        for ((category, moduleList) in byCategories.asMap().entries) {
            val values = JsonArray()
            moduleList.forEach {
                values.add(JsonPrimitive(it))
            }
            jsonObj.add(category, values)
        }

        callback?.success(jsonObj.toString())
    }
}

class ModuleNamesArguments(parser: ArgParser) {

}