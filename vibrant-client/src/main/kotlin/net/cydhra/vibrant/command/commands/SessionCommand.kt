package net.cydhra.vibrant.command.commands

import com.google.gson.GsonBuilder
import com.xenomachina.argparser.ArgParser
import com.xenomachina.argparser.default
import net.cydhra.nidhogg.YggdrasilClient
import net.cydhra.nidhogg.data.Session
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

/**
 * A command to set a serialized session as the active session
 */
object SessionCommand : CommandHandler<SessionArguments>(userCommand = false) {
    private val gson = GsonBuilder().create()

    override fun executeCommand(origin: CommandSender, label: String, arguments: SessionArguments, callback: CefQueryCallback?) {
        if (arguments.extract) {
            callback?.success(gson.toJson(VibrantClient.minecraft.minecraftSession))
            return
        }

        if (arguments.session.isEmpty()) {
            callback?.failure(-1, "argument SESSION can only be empty with -x")
            return
        }

        val session = gson.fromJson(arguments.session.joinToString(" "), Session::class.java)

        when {
            arguments.apply -> {
                if (arguments.autorefresh) {
                    with(YggdrasilClient(session.clientToken)) {
                        if (!validate(session)) {
                            refresh(session)
                        }
                    }
                }

                VibrantClient.minecraft.minecraftSession = session
                callback?.success(gson.toJson(session))
            }
            arguments.validate -> callback?.success(YggdrasilClient(nidhoggClientToken = session.clientToken).validate(session).toString())
            arguments.invalidate -> {
                YggdrasilClient(nidhoggClientToken = session.clientToken).invalidate(session)
                callback?.success("Successfully invalidated session")
            }
            arguments.refresh -> {
                try {
                    YggdrasilClient(session.clientToken).refresh(session)
                } catch (e: Exception) {
                    callback?.success("false")
                    return
                }
                callback?.success(gson.toJson(session))
            }
        }
    }
}

class SessionArguments(parser: ArgParser) {
    val apply by parser.flagging("-a", "--apply", help = "If set, the session will be set active in the client. Returns the session as JSON.")
    val validate by parser.flagging("-v",
            "--validate",
            help = "If set, the session is validated and true is returned, if it is still valid"
    )
    val invalidate by parser.flagging("-i", "--invalidate", help = "If set, the session is invalidated")
    val refresh by parser.flagging("-r", "--refresh", help = "If set, the session is refreshed")
    val extract by parser.flagging("-x", "--extract", help = "Extract the currently set session. Can be used to identify the currently " +
            "logged in player or integrate its session into a manager."
    )

    val autorefresh by parser.flagging("--autorefresh", help = "On apply the session is validated and refreshed automatically.")

    val session by parser.positionalList("SESSION",
            help = "JSON: Instance of net.cydhra.nidhogg.data.Session. Not required by -x"
    ).default(emptyList())
}
