package net.cydhra.vibrant.command.commands

import com.xenomachina.argparser.ArgParser
import net.cydhra.vibrant.command.CommandDispatcher
import net.cydhra.vibrant.command.CommandHandler
import net.cydhra.vibrant.command.CommandSender
import org.cef.callback.CefQueryCallback

/**
 * Re-dispatches commands with the user as [CommandSender]
 */
object UserCommand : CommandHandler<String>(userCommand = false, proxyCommand = true) {
    override fun executeCommand(origin: CommandSender, label: String, arguments: String, callback: CefQueryCallback?) {
        CommandDispatcher.dispatchCommand(arguments, CommandSender.PLAYER, callback)
    }
}