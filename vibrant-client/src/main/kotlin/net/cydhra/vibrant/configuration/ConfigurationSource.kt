package net.cydhra.vibrant.configuration

import com.uchuhimo.konf.Config

interface ConfigurationSource {

    fun applySource(config: Config): Config
}