package net.cydhra.vibrant.configuration

import com.uchuhimo.konf.Config
import java.io.File

/**
 * Adds a json file as a config source
 */
class ConfigurationFileSource(private val file: File) : ConfigurationSource {
    override fun applySource(config: Config): Config {
        return config.from.json.file(this.file)
    }
}