@file:Suppress("UNUSED_PARAMETER")

package net.cydhra.vibrant

import net.cydhra.eventsystem.EventManager
import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.eventsystem.listeners.ListenerPriority
import net.cydhra.vibrant.api.client.VibrantFactory
import net.cydhra.vibrant.api.client.VibrantMinecraft
import net.cydhra.vibrant.api.render.VibrantGlStateManager
import net.cydhra.vibrant.configuration.ConfigurationService
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.events.render.RenderOverlayEvent
import net.cydhra.vibrant.events.render.RenderWorldEvent
import net.cydhra.vibrant.modulesystem.BypassMode
import net.cydhra.vibrant.modulesystem.LocalModuleLoader
import net.cydhra.vibrant.modulesystem.ModuleManager
import net.cydhra.vibrant.util.shader.ShaderLibrary
import org.apache.logging.log4j.LogManager

/**
 *
 */
object VibrantClient {

    const val VERSION = "1.0"
    val AUTH_TOKEN = "Vibrant"

    val logger = LogManager.getLogger()

    lateinit var minecraft: VibrantMinecraft
        private set

    lateinit var factory: VibrantFactory
        private set

    val glStateManager: VibrantGlStateManager
        get() = minecraft.glStateManager

    var bypassMode: BypassMode = BypassMode.VANILLA

    const val DEBUG: Boolean = true

    fun init(minecraft: VibrantMinecraft, factory: VibrantFactory) {
        logger.info("Vibrant client successfully hooked")
        this.minecraft = minecraft
        this.factory = factory

        ConfigurationService.init()

        logger.info("loading shaders")
        ShaderLibrary // loading is handled inside, so just init the object

        logger.info("loading modules")
        ModuleManager.registerModuleLoader(LocalModuleLoader())
        logger.info("initialize modules")
        ModuleManager.init()

        logger.info("loading configuration")
        ConfigurationService.reloadConfig()

        logger.info("registering listeners")
        EventManager.registerListeners(this)

        logger.info("saving configuration")
        ConfigurationService.saveConfig()
        logger.info("initialization completed")
    }

    fun tick() {
        EventManager.callEvent(MinecraftTickEvent())
    }

    @EventHandler(priority = ListenerPriority.HIGHEST)
    fun beforeRenderOverlay(e: RenderOverlayEvent) {
        glStateManager.pushAttrib()
    }

    @EventHandler(priority = ListenerPriority.LOWEST)
    fun afterRenderOverlay(e: RenderOverlayEvent) {
        minecraft.getTextureManagerInstance().bindTexture("textures/gui/icons.png")
        glStateManager.popAttrib()
        glStateManager.enableBlend()
    }

    @EventHandler(priority = ListenerPriority.HIGHEST)
    fun beforeRenderWorld(e: RenderWorldEvent) {
        glStateManager.pushAttrib()
    }

    @EventHandler(priority = ListenerPriority.LOWEST)
    fun afterRenderWorld(e: RenderWorldEvent) {
        glStateManager.popAttrib()
        minecraft.getTextureManagerInstance().bindTexture("textures/gui/icons.png")
    }
}