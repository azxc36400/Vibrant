package net.cydhra.vibrant.gui.util.fontrenderer

import java.awt.Font
import java.util.*

/**
 * Class for maintaining font renderers
 *
 * @author Flaflo
 */
object FontRenderers {

    private val cache: MutableMap<Int, VibrantFontRenderer> = HashMap()

    /**
     * @param font the font to get the renderer for
     * @return the renderer for this font
     */
    operator fun get(font: Font): VibrantFontRenderer {
        val hash = font.hashCode()
        var fontRenderer: VibrantFontRenderer? = this.cache[hash]

        if (fontRenderer == null) {
            fontRenderer = VibrantFontRenderer(font)
            this.cache[hash] = fontRenderer
        }

        return fontRenderer
    }

    /**
     * @param name the name of the font
     * @param style the style of the font
     * @param size the size of the font
     * @return the renderer for this font
     */
    operator fun get(name: String, style: Int, size: Int): VibrantFontRenderer {
        return this[Fonts.INSTANCE.get(name, style, size)]
    }
}
