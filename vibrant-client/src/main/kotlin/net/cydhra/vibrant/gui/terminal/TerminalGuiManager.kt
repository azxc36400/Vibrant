package net.cydhra.vibrant.gui.terminal

import net.cydhra.vibrant.gui.AbstractVibrantGuiManager

object TerminalGuiManager : AbstractVibrantGuiManager("terminal")