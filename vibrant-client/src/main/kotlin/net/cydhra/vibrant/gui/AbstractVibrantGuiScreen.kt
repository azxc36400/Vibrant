package net.cydhra.vibrant.gui

import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.api.client.VibrantMinecraft
import net.cydhra.vibrant.api.gui.VibrantGuiController
import net.cydhra.vibrant.api.gui.VibrantGuiScreen
import org.lwjgl.input.Keyboard
import org.lwjgl.input.Mouse
import java.awt.Color
import java.awt.event.InputEvent
import java.util.*
import kotlin.collections.HashSet


/**
 * This class extends [VibrantGuiScreen] to override all the properties that must be implemented but are initialized by the adapter class.
 */
abstract class AbstractVibrantGuiScreen(protected val controller: VibrantGuiController) : VibrantGuiScreen {

    override var height: Int = -1
    override var width: Int = -1
    private val buttonsPressed = HashSet<Int>()

    private var isInitialized = false

    protected val mc: VibrantMinecraft
        get() = VibrantClient.minecraft

    abstract val guiModule: AbstractVibrantGuiManager

    constructor() : this(VibrantClient.factory.newGuiController())

    override fun initGui() {
        if (!isInitialized) {
            this.initGuiFirstTime()
            isInitialized = true
        }
    }

    /**
     * Called when the GUI is initialized (on open) for the first time
     */
    open fun initGuiFirstTime() {
        guiModule.browser.resize(mc.displayWidth, mc.displayHeight)
    }

    override fun drawScreen(mouseX: Int, mouseY: Int, partialTicks: Float) {
        this.handleInput()
        guiModule.browser.mcefUpdate()
        VibrantClient.glStateManager.color(Color.WHITE)
        guiModule.browser.draw(0.0, height.toDouble(), width.toDouble(), 0.0)
    }

    override fun tickScreen() {
        guiModule.tickModule()
    }

    override fun onResize(mcIn: VibrantMinecraft, width: Int, height: Int) {
        guiModule.browser.resize(mc.displayWidth, mc.displayHeight)
    }

    override fun handleInput() {
        var mods = 0
        if (Keyboard.isKeyDown(Keyboard.KEY_LCONTROL) || Keyboard.isKeyDown(Keyboard.KEY_RCONTROL))
            mods = mods or InputEvent.CTRL_DOWN_MASK
        if (Keyboard.isKeyDown(Keyboard.KEY_LSHIFT) || Keyboard.isKeyDown(Keyboard.KEY_RSHIFT))
            mods = mods or InputEvent.SHIFT_DOWN_MASK
        if (Keyboard.isKeyDown(Keyboard.KEY_LMENU))
            mods = mods or InputEvent.ALT_DOWN_MASK
        if (Keyboard.isKeyDown(Keyboard.KEY_RMENU))
            mods = mods or InputEvent.ALT_GRAPH_DOWN_MASK
        if (Keyboard.isKeyDown(Keyboard.KEY_LMETA) || Keyboard.isKeyDown(Keyboard.KEY_RMETA))
            mods = mods or InputEvent.META_DOWN_MASK

        while (Keyboard.next()) {
            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE) {
                escapeAction()
            }

            val pressed = Keyboard.getEventKeyState()
            val key = Keyboard.getEventCharacter()

            if (pressed)
                guiModule.browser.injectKeyPressed(key, mods)
            else
                guiModule.browser.injectKeyReleased(key, mods)

            if (key.toInt() != Keyboard.CHAR_NONE)
                guiModule.browser.injectKeyTyped(key, mods)
        }

        while (Mouse.next()) {
            val btn = when(Mouse.getEventButton()) {
                -1 -> -1
                0 -> 1
                1 -> 3
                2 -> 2
                else -> Mouse.getEventButton() + 1
            }

            val pressed = Mouse.getEventButtonState()
            val sx = Mouse.getEventX()
            val sy = Mouse.getEventY()

            if (pressed) {
                buttonsPressed.add(btn)
            } else {
                buttonsPressed.remove(btn)
            }

            val y = mc.displayHeight - sy

            if (btn == -1) {
                guiModule.browser.injectMouseMove(sx, y, mods, !buttonsPressed.isEmpty())
            }
            else
                guiModule.browser.injectMouseButton(sx, y, mods, btn, pressed, 1)
        }
    }

    open fun escapeAction() {
        VibrantClient.minecraft.displayGuiScreen(null)
    }
}