package net.cydhra.vibrant.gui.util.fontrenderer

import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.api.render.VibrantDynamicTexture
import java.awt.Color
import java.awt.Font
import java.awt.Graphics2D
import java.awt.RenderingHints
import java.awt.image.BufferedImage

/**
 * Renderer used to draw any Font in minecraft/lwjgl
 *
 * @author Flaflo
 */
class VibrantFontRenderer(val font: Font) {

    private class Glyph(val character: Char, val atlasX: Double, val atlasY: Double, val width: Double, val height: Double)


    companion object {
        /**
         * How many chars in a row inside the atlas
         */
        private val MAX_CHARS_PER_ROW = 14

        /**
         * Defines the char to start with adding to the atlas
         */
        private val FIRST_CHAR = 32

        /**
         * Defines the last char added to the atlas
         */
        private val LAST_CHAR = 255

        /**
         * Defines the gap between chars in the atlas
         */
        private val GAP = 10
    }

    //Because Cydhra is bad!
    private val guiController = VibrantClient.factory.newGuiController()

    private var fontAtlas: VibrantDynamicTexture? = null
    private var atlasWidth: Int = 0
    private var atlasHeight: Int = 0

    private var glyphs: Array<Glyph?>

    var fontHeight: Double = 0.0

    init {
        this.glyphs = arrayOfNulls(LAST_CHAR - FIRST_CHAR)

        this.setupFontAtlas()
    }

    /**
     * Creates a font atlas containing all chars to be drawn
     */
    private fun setupFontAtlas() {
        var image = BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB)
        var graphics2d = image.graphics as Graphics2D
        graphics2d.font = this.font

        val fontMetrics = graphics2d.fontMetrics

        // Calculate atlas width and height
        var indexInRow = 0
        var currentRowWidth = 0
        var maxRowWidth = 0
        var maxRowHeight = 0
        var rowCount = 0

        for (i in FIRST_CHAR until LAST_CHAR) {
            indexInRow++

            if (indexInRow == MAX_CHARS_PER_ROW - 1) {
                indexInRow = 0
                maxRowWidth = Math.max(maxRowWidth, currentRowWidth)
                currentRowWidth = 0
                maxRowHeight = Math.max(maxRowHeight,
                        fontMetrics.ascent + fontMetrics.descent + this.font.size / 2
                )
                rowCount++
            }

            currentRowWidth += fontMetrics.stringWidth(Character.toString(i.toChar())) + GAP
        }

        this.atlasWidth = maxRowWidth

        for (i in 0 until rowCount) {
            this.atlasHeight += maxRowHeight
        }

        graphics2d.dispose()
        image = BufferedImage(this.atlasWidth, this.atlasHeight, BufferedImage.TYPE_4BYTE_ABGR)

        graphics2d = image.graphics as Graphics2D

        graphics2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON)
        graphics2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON)

        graphics2d.font = this.font
        graphics2d.color = Color.WHITE

        // Fill atlas with chars
        // Important parts of this code could only be achieved by studying other
        // renderers, because of the poor documentation of the java font metrics
        // Worth mentioning is NahrFont, which's code is absolutely terrible
        var x = 0f
        var y = 0f

        for (i in FIRST_CHAR until LAST_CHAR) {
            if (x >= this.atlasWidth - fontMetrics.maxAdvance / 2) {
                x = 0f
                y += (fontMetrics.ascent + fontMetrics.descent + this.font.size / 2).toFloat()
            }

            val chr = i.toChar()
            val strChr = Character.toString(chr)

            val bounds = fontMetrics.getStringBounds(Character.toString(chr), graphics2d)

            this.glyphs[i - FIRST_CHAR] = Glyph(chr, (x - GAP).toDouble(), (y - fontMetrics.descent).toDouble(), bounds.width + GAP,
                    bounds.height + fontMetrics.descent
            )

            graphics2d.drawString(strChr, x, y + fontMetrics.ascent)

            x += (fontMetrics.stringWidth(strChr) + GAP).toFloat()
        }

        graphics2d.dispose()

        this.fontHeight = fontMetrics.height.toDouble() / 2 - 1
        this.fontAtlas = VibrantClient.factory.newDynamicTexture(image)
    }

    /**
     * Deletes the font renderer and makes it unusable
     */
    fun dispose() {
        this.fontAtlas!!.deleteGlTexture()
    }

    /**
     * Draws a string with a shadow colored darker than the original one
     *
     * @param string the string to draw
     * @param x the x position
     * @param y the y position
     * @param color the color to use
     */
    fun drawStringWithShadow(string: String, x: Double, y: Double, color: Color) {
        VibrantClient.glStateManager.translate(0.5, 0.5, 0.0)
        this.drawString(string, x, y, Color.BLACK)
        VibrantClient.glStateManager.translate(-0.5, -0.5, 0.0)
        this.drawString(string, x, y, color)
    }

    /**
     * Draws a string
     *
     * @param string the string to draw
     * @param x the x position
     * @param y the y position
     * @param color the color to use
     */
    fun drawString(string: String, x: Double, y: Double, color: Color) {
        var x = x
        var y = y
        x *= 2.0
        y *= 2.0
        y -= this.fontHeight / 2.0

        VibrantClient.glStateManager.color(color)
        VibrantClient.glStateManager.bindTexture(this.fontAtlas!!.getGlTextureId())

        VibrantClient.glStateManager.pushMatrix()
        VibrantClient.glStateManager.scale(0.5, 0.5, 0.0)

        for (chr in string.toCharArray()) {
            val glyph = this.getGlyph(chr)

            val width = glyph!!.width
            val height = glyph!!.height

            val atlasX = glyph!!.atlasX
            val atlasY = glyph!!.atlasY

            guiController.drawRectWithCustomSizedTexture((x - GAP.toDouble() - 1.0).toInt(),
                    (y - 2).toInt(),
                    atlasX.toFloat(),
                    atlasY.toFloat(),
                    width.toInt(),
                    height.toInt(),
                    this.atlasWidth.toFloat(),
                    this.atlasHeight.toFloat()
            )

            x += width - GAP
        }

        VibrantClient.glStateManager.popMatrix()
    }

    /**
     * @param string the string to get the width of
     * @return the width of the provided string
     */
    fun getStringWidth(string: String): Double {
        val width = string.toCharArray()
                .map { this.getGlyph(it) }
                .sumByDouble { it!!.width - GAP }

        return width / 2
    }

    /**
     * @param chr the related char
     * @return the glyph object related to the char given
     */
    private fun getGlyph(chr: Char): Glyph? {
        return this.glyphs[chr.toInt() - FIRST_CHAR]
    }
}
