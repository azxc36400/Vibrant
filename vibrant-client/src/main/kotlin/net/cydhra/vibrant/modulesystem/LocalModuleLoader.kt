package net.cydhra.vibrant.modulesystem

import com.google.common.reflect.ClassPath
import com.uchuhimo.konf.ConfigSpec
import net.cydhra.vibrant.configuration.ConfigurationService
import net.cydhra.vibrant.configuration.getValue

private const val MODULE_PARENT_PACKAGE = "net.cydhra.vibrant.modules"

/**
 * [ModuleLoader] for modules provided in [MODULE_PARENT_PACKAGE].
 *
 * @see [loadModules]
 */
class LocalModuleLoader : ModuleLoader {

    companion object : ConfigSpec("localmoduleloader") {
        val enabledModules by optional<MutableMap<String, Boolean>>(mutableMapOf())
    }

    val moduleMap by enabledModules

    /**
     * Load modules according to config. All modules from the package [MODULE_PARENT_PACKAGE] and its subpackages are traversed and if the
     * config does not forbid it, they will be registered in the [ModuleManager]
     */
    override fun loadModules(): List<Module> {
        ConfigurationService.addSpecification(LocalModuleLoader)
        ConfigurationService.reloadConfig()

        val moduleList = mutableListOf<Module>()

        @Suppress("UnstableApiUsage")
        moduleList += ClassPath
                .from(this.javaClass.classLoader)
                .getTopLevelClassesRecursive(MODULE_PARENT_PACKAGE)
                .map { it.load() }
                .filter { Module::class.java.isAssignableFrom(it) }
                .map { it.newInstance() as Module }
                .filter { module ->
                    if (moduleMap.containsKey(module.name)) {
                        return@filter moduleMap[module.name]!!
                    } else {
                        moduleMap[module.name] = true
                        return@filter true
                    }
                }
                .toList()

        return moduleList
    }
}