package net.cydhra.vibrant.modulesystem

import com.uchuhimo.konf.ConfigSpec
import net.cydhra.eventsystem.EventManager
import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.VibrantClient.logger
import net.cydhra.vibrant.configuration.ConfigurationService
import net.cydhra.vibrant.configuration.getValue
import net.cydhra.vibrant.events.minecraft.KeyboardActionEvent
import net.cydhra.vibrant.modulesystem.ModuleManager.init
import net.cydhra.vibrant.modulesystem.ModuleManager.onKeyEvent

/**
 * A registry for [Module] implementations. They will get registered in [init] and are enabled when [onKeyEvent] handles a fitting [KeyboardActionEvent]
 */
object ModuleManager {

    private object ModuleManagerSettings : ConfigSpec("manager.modules") {
        val enabledModules by optional<MutableMap<String, Boolean>>(mutableMapOf())
    }

    private val enabledModules by ModuleManagerSettings.enabledModules

    private val registeredModuleLoaders = mutableListOf<ModuleLoader>()
    private val registeredModules = mutableListOf<Module>()

    val modules: List<Module> = registeredModules

    /**
     * Initialize manager. Registers [KeyboardActionEvent] handler and register all modules.
     */
    fun init() {
        logger.info("initializing module manager")
        EventManager.registerListeners(this)

        logger.info("loading module manager configuration")
        ConfigurationService.addSpecification(ModuleManagerSettings)
        ConfigurationService.reloadConfig()

        /*
         * A while loop is used, because modules are explicitly allowed to add more module loaders thus modifying the list.
         */
        var index = 0
        while (index < this.registeredModuleLoaders.size) {
            this.registerModules(this.registeredModuleLoaders[index++].loadModules().also { modules ->
                modules.forEach(Module::initialize)
                modules.forEach { mod ->
                    // enable all modules that are enabled in config
                    mod.isEnabled = enabledModules.putIfAbsent(mod.name, mod.isEnabled) ?: mod.isEnabled
                }
            })
        }

        registeredModules.sortWith(kotlin.Comparator { m1: Module, m2: Module -> m2.displayName.length - m1.displayName.length })
    }

    private fun registerModules(modules: Collection<Module>) {
        logger.debug("registering modules ${modules.map { it.name }.joinToString(", ")}")
        this.registeredModules += modules
    }

    /**
     * Register a module
     * @param module module to be registered
     */
    private fun registerModule(module: Module) {
        logger.debug("registering module ${module.name}")
        this.registeredModules += module
    }

    fun registerModuleLoader(moduleLoader: ModuleLoader) {
        logger.debug("registering module loader ${moduleLoader.javaClass.simpleName}")
        this.registeredModuleLoaders += moduleLoader
    }

    @EventHandler
    fun onKeyEvent(e: KeyboardActionEvent) {
        if (e.type == KeyboardActionEvent.KeyboardAction.PRESS) {
            this.modules.filter { it.keybind.keyCode == e.keycode }.forEach(Module::toggle)
        }
    }

    /**
     * Update the map of enabled modules and save the configuration
     */
    fun updateModuleState() {
        this.modules.forEach { mod ->
            this.enabledModules[mod.name] = mod.isEnabled
        }
        ConfigurationService.saveConfig()
    }
}