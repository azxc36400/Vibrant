package net.cydhra.vibrant.api.world

interface VibrantBlockPosition {

    val posX: Int
    val posY: Int
    val posZ: Int

    fun offsetBy(x: Double, y: Double, z: Double): VibrantBlockPosition

    fun offsetBy(x: Int, y: Int, z: Int): VibrantBlockPosition

    fun offsetUp(): VibrantBlockPosition

    fun offsetUp(count: Int): VibrantBlockPosition

    fun offsetDown(): VibrantBlockPosition

    fun offsetDown(count: Int): VibrantBlockPosition

    fun offsetNorth(): VibrantBlockPosition

    fun offsetNorth(count: Int): VibrantBlockPosition

    fun offsetEast(): VibrantBlockPosition

    fun offsetEast(count: Int): VibrantBlockPosition

    fun offsetSouth(): VibrantBlockPosition

    fun offsetSouth(count: Int): VibrantBlockPosition

    fun offsetWest(): VibrantBlockPosition

    fun offsetWest(count: Int): VibrantBlockPosition

    fun offsetSide(facing: VibrantBlockFacing): VibrantBlockPosition

    fun offsetSide(facing: VibrantBlockFacing, count: Int): VibrantBlockPosition
}

enum class VibrantBlockFacing(private val oppositeIndex: Int, val horizontalIndex: Int, val axisDirection: VibrantAxisDirection, val axis: VibrantAxis,
        val offsetVec: Triple<Int, Int, Int>) {
    DOWN(1, -1, VibrantAxisDirection.NEGATIVE, VibrantAxis.Y, Triple(0, -1, 0)),
    UP(0, -1, VibrantAxisDirection.POSITIVE, VibrantAxis.Y, Triple(0, 1, 0)),
    NORTH(3, 2, VibrantAxisDirection.NEGATIVE, VibrantAxis.Z, Triple(0, 0, -1)),
    SOUTH(2, 0, VibrantAxisDirection.POSITIVE, VibrantAxis.Z, Triple(0, 0, 1)),
    WEST(5, 1, VibrantAxisDirection.NEGATIVE, VibrantAxis.X, Triple(-1, 0, 0)),
    EAST(4, 3, VibrantAxisDirection.POSITIVE, VibrantAxis.X, Triple(1, 0, 0));

    fun opposite(): VibrantBlockFacing = VibrantBlockFacing.values()[this.oppositeIndex]
}


enum class VibrantAxis {
    X, Y, Z
}

enum class VibrantAxisDirection(val directionOffset: Int) {
    NEGATIVE(-1),
    POSITIVE(1)
}
