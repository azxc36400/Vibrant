package net.cydhra.vibrant.api.entity

import net.cydhra.vibrant.api.util.VibrantBoundingBox
import net.cydhra.vibrant.api.util.VibrantVec3
import net.cydhra.vibrant.api.world.VibrantBlockFacing

/**
 *
 */
interface VibrantEntity : VibrantEntityAlike {
    val prevPosX: Double
    val prevPosY: Double
    val prevPosZ: Double

    var motionX: Double
    var motionY: Double
    var motionZ: Double

    val rotationYaw: Float
    val rotationPitch: Float

    var onGround: Boolean
    var fallDistance: Float
    var isEntitySprinting: Boolean

    var width: Float
    var height: Float

    val boundingBox: VibrantBoundingBox

    var stepHeight: Float

    @Deprecated("Seems to be always null")
    val facing: VibrantBlockFacing
    val horizontalBlockFacing: VibrantBlockFacing

    val isCollidedHorizontally: Boolean
    val isCollidedVertically: Boolean

    fun getDistanceSquared(x: Double, y: Double, z: Double): Double

    fun getEntityName(): String

    fun setEntityPosition(posX: Double, posY: Double, posZ: Double)

    fun setEntityPositionAndRotation(posX: Double, posY: Double, posZ: Double, yaw: Float, pitch: Float)

    fun setEntityRotation(yaw: Float, pitch: Float)

    fun setEntityLocationAndAngles(posX: Double, posY: Double, posZ: Double, yaw: Float, pitch: Float)

    fun getLocationVector(): VibrantVec3

    fun getEntityEyeHeight(): Float
}