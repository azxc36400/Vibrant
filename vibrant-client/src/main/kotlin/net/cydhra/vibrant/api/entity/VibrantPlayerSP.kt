package net.cydhra.vibrant.api.entity

import net.cydhra.vibrant.api.client.VibrantMovementInput
import net.cydhra.vibrant.api.inventory.VibrantContainer
import net.cydhra.vibrant.api.item.VibrantItemStack
import net.cydhra.vibrant.api.util.chat.VibrantChatComponent

/**
 *
 */
interface VibrantPlayerSP : VibrantPlayer {

    val movementInput: VibrantMovementInput

    fun swing()

    fun sendChatMessageToServer(message: String)

    fun displayChatMessageOnClient(message: VibrantChatComponent)
}