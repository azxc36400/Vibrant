package net.cydhra.vibrant.api.item

/**
 *
 */
interface VibrantItem {

    fun getEnchantability(): Int
}