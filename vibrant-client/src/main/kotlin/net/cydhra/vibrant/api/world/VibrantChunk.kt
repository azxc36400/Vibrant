package net.cydhra.vibrant.api.world

interface VibrantChunk {

    fun getBlockInChunk(x: Int, y: Int, z: Int): VibrantBlockInfo
}