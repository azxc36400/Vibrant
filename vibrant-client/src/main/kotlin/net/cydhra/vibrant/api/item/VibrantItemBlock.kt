package net.cydhra.vibrant.api.item

import net.cydhra.vibrant.api.world.VibrantBlockInfo

interface VibrantItemBlock : VibrantItem {
    val blockInfo: VibrantBlockInfo
}