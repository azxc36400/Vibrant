package net.cydhra.vibrant.api.world.storage

interface VibrantSaveFormat {

    /**
     * Rename a world. Does not rename the folder where the world is saved in.
     *
     * @param worldName current name of the world
     * @param newName new name of the world
     */
    fun rename(worldName: String, newName: String)

    /**
     * @param folder name of the save folder to delete
     *
     * @return true, if successfully deleted the world
     */
    fun deleteDirectory(folder: String): Boolean

    /**
     * @return a list of world saves
     */
    fun getSaves(): List<VibrantSaveFormatComparator>
}