package net.cydhra.vibrant.api.entity

import net.cydhra.vibrant.api.inventory.VibrantContainer
import net.cydhra.vibrant.api.inventory.VibrantPlayerInventory
import net.cydhra.vibrant.api.item.VibrantItemStack

/**
 *
 */
interface VibrantPlayer : VibrantEntityLiving {

    val chasingPosX: Double
    val chasingPosY: Double
    val chasingPosZ: Double

    var isAllowedFlying: Boolean
    var isFlying: Boolean

    val playerInventory: VibrantPlayerInventory

    val openContainer: VibrantContainer

    fun getItemUsingCounter(): Int

    fun getItemHeld(): VibrantItemStack?
}