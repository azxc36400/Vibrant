package net.cydhra.vibrant.modules.gui

import net.cydhra.vibrant.gui.GuiQueryHandler
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.GuiModuleLoader
import net.cydhra.vibrant.modulesystem.Module
import net.cydhra.vibrant.modulesystem.ModuleManager

/**
 * This module enables the client's custom GUI capabilities
 */
class GuiModule : Module("GuiModule", DefaultCategories.SYSTEM) {

    override fun initialize() {
        this.isEnabled = true
    }

    override fun onEnable() {
        GuiQueryHandler.initialize()

        // register a special module loader to load further modules for the GUI
        ModuleManager.registerModuleLoader(GuiModuleLoader())
    }
}