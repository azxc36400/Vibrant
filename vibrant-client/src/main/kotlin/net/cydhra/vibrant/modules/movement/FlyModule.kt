package net.cydhra.vibrant.modules.movement

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.VibrantClient
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module
import org.lwjgl.input.Keyboard

class FlyModule : Module("Fly", DefaultCategories.MOVEMENT, Keyboard.KEY_F) {

    override fun onDisable() {
        mc.thePlayer!!.isFlying = false

        // TODO set isAllowedFlying to false, if player is not in creative mode
    }

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (mc.thePlayer == null)
            return

        if (!VibrantClient.minecraft.thePlayer!!.onGround) {
            mc.thePlayer!!.isFlying = true
            mc.thePlayer!!.isAllowedFlying = true
        }
    }
}