package net.cydhra.vibrant.modules.movement

import net.cydhra.eventsystem.listeners.EventHandler
import net.cydhra.vibrant.api.world.blocks.VibrantBlockStairs
import net.cydhra.vibrant.configuration.ConfigurationService
import net.cydhra.vibrant.configuration.ModuleConfigurationSpec
import net.cydhra.vibrant.configuration.getValue
import net.cydhra.vibrant.events.minecraft.MinecraftTickEvent
import net.cydhra.vibrant.modulesystem.DefaultCategories
import net.cydhra.vibrant.modulesystem.Module


/**
 * A module that speeds up on ladders, stairs, etc
 */
class TerrainSpeedModule : Module("TerrainSpeed", DefaultCategories.MOVEMENT) {

    companion object : ModuleConfigurationSpec("TerrainSpeed") {
        val ladder by optional(true)
        val stairs by optional( true)
        val ladderSpeed by optional( 2.5f)
        val stairsSpeed by optional( 1.5f)
    }

    val ladder by Companion.ladder
    val stairs by Companion.stairs
    val ladderSpeed by Companion.ladderSpeed
    val stairsSpeed by Companion.stairsSpeed

    init {
        ConfigurationService.addSpecification(TerrainSpeedModule)
    }

    @EventHandler
    fun onTick(e: MinecraftTickEvent) {
        if (mc.thePlayer != null) {
            if (ladder) {
                if (mc.thePlayer!!.isCollidedHorizontally && mc.thePlayer!!.isClimbing()) {
                    mc.thePlayer!!.motionY *= ladderSpeed
                }
            }
            if (stairs) {
                val playerStandingPosition =
                        factory.newBlockPosition(mc.thePlayer!!.posX.toInt(), mc.thePlayer!!.posY.toInt() - 1, mc.thePlayer!!.posZ.toInt())
                if (mc.theWorld!!.getBlockInfoAt(playerStandingPosition) is VibrantBlockStairs) {
                    if(mc.thePlayer!!.onGround) {
                        mc.thePlayer!!.motionX *= stairsSpeed
                        mc.thePlayer!!.motionZ *= stairsSpeed
                    }
                }
            }
        }
    }
}
