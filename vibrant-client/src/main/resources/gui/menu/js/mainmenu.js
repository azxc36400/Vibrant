function _(el) {
    return document.querySelector(el);
}

const heading = _("#heading");
const buttonWarapper = _("#buttonWarapper");

const buttonSingleplayer = _("#buttonSingleplayer");
const buttonMultiplayer = _("#buttonMultiplayer");
const buttonSettings = _("#buttonSettings");
const buttonLanguage = _("#buttonLanguage");
const buttonQuit = _("#buttonQuit");
const altManager = _("#altManager");

buttonSingleplayer.addEventListener("click", () => {
    window.vibrantQuery( {'request': 'singleplayer'});
});

buttonMultiplayer.addEventListener("click", () => {
    window.vibrantQuery( {'request': 'multiplayer'});
});

buttonSettings.addEventListener("click", () => {
    window.vibrantQuery( {'request': 'options'} );
});

buttonLanguage.addEventListener("click", () => {
    window.vibrantQuery( {'request': 'selectLanguage'})
});

altManager.addEventListener("click", () => {
    window.location = "AltManager.html";
}); 

buttonQuit.addEventListener("click", () => {
    window.vibrantQuery( {'request': 'quit'} )
}); 

window.addEventListener("load", () => {

    setTimeout(() => {
        heading.style.marginTop = "80px";
        heading.style.opacity = ".7";

        buttonWarapper.style.opacity = ".7";
    }, 200);
});