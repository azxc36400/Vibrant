/**
 * Returns the respective DOM object for a given identifier.
 *
 * @param {String} el - DOM element referred to, eg. by ID or class name
 */
function _(el) {
    return document.querySelector(el);
}

/**
 * Logs a string to the Minecraft output.
 *
 * @param {String} s - String to be logged
 */
function log(s) {
    window.vibrantQuery({
        "request": "echo " + s,
        "onSuccess": function (response) {
        },
        "onFailure": function (code, response) {
            console.log(response);
        }
    });
}

/**
 * Initializes the ClickGUI.
 *
 * @param {Object} config - The object which contains the values of the ClickGUI. { Category1: ["Module1", "Module2", ...], Category2: ["Module1", "Module2", ...], ... } 
 */
function initialize(config) {
    // Creates a ClickGUI panel for each category,
    Object.entries(config).forEach(category => {
        var panel = document.createElement("div");
        panel.classList.add("panel");

        var blur = document.createElement("div");
        blur.classList.add("blur");
        panel.appendChild(blur);

        var title = document.createElement("div");
        title.classList.add("title");
        title.innerHTML = category[0];
        blur.appendChild(title);

        var inner = document.createElement("div");
        inner.classList.add("inner");
        blur.appendChild(inner);

        var modules = category[1].sort();

        for (var i = 0; i < modules.length; i++) {
            var cModule = document.createElement("div");
            cModule.classList.add("module");
            cModule.innerHTML = category[1][i];
            inner.appendChild(cModule);
        }

        clickgui.appendChild(panel);


        var panels = document.getElementsByClassName("panel");
        var titleHeight = panels[0].getElementsByClassName("title")[0].clientHeight;
        var blurs = document.getElementsByClassName("blur");

        for (var i = 0; i < blurs.length; i++) {
            blurs[i].style.maxHeight = `${titleHeight}px`;
            panels[i].style.top = `${i * (titleHeight + 10) + 10}px`;
        }
    });
}

/**
 * Calculates and returns the CSS style values of a given DOM onject. (WARNING: May be inaccurate!)
 *
 * @param {Object} el - DOM object of whose style will be calculated.
 * @returns {Object} - Object containg the CSS styling information of the element.
 */
function calcStyle(el) {
    return el.currentStyle || window.getComputedStyle(el);
}

/**
 * Removes a given DOM object from the document body.
 *
 * @param {Object} el - The DOM object to be deleted
 */
function removeElement(el) {
    el.parentElement.removeChild(el);
}

/**
 * Sorts a module options object.
 *
 * @param {Object} settings - The settings object to be sorted.
 */
function sortSettings(settings) {
    var rangeSettings = {};
    var enumSettings = {};
    var booleanSettings = {};

    Object.entries(settings).forEach(
        ([key, value]) => {
            switch (value.type.toLowerCase()) {
                case "range":
                    rangeSettings[key] = value;

                    break;

                case "enum":
                    enumSettings[key] = value;

                    break;

                case "boolean":
                    booleanSettings[key] = value;

                    break;
            }
        }
    );

    var sortedSettings = Object.assign({}, rangeSettings, enumSettings, booleanSettings);

    return sortedSettings;
}

/**
 * Enabled a given module in the ClickGUI.
 *
 * @param {Object} element - The DOM object of the module to be toggled.
 * @param {Boolean} state - State the module should be toggles to.
 */
function toggleModule(element, state) {
    var moduleName = element.innerHTML;

    if (state) {
        window.vibrantQuery({
            "request": "toggle --enable " + moduleName,
            "onSuccess": function (response) {
                element.classList.add("enabled");
            },
            "onFailure": function (code, response) {
                console.log(response);
            }
        });

    } else {
        window.vibrantQuery({
            "request": "toggle --disable " + moduleName,
            "onSuccess": function (response) {
                element.classList.remove("enabled");
            },
            "onFailure": function (code, response) {
                console.log(response);
            }
        });
    }
}

/**
 * Makes an element draggable.
 *
 * @param {Object} element - The DOM object that will be made draggable.
 * @see [Credits]{@link https://www.w3schools.com/howto/tryit.asp?filename=tryhow_js_draggablem}
 */
function dragElement(element) {
    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

    element.getElementsByClassName("title")[0].onmousedown = dragMouseDown;

    function dragMouseDown(e) {
        e = e || window.event;
        e.preventDefault();
        pos3 = e.clientX;
        pos4 = e.clientY;
        document.onmouseup = closeDragElement;
        document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
        e = e || window.event;
        e.preventDefault();
        pos1 = pos3 - e.clientX;
        pos2 = pos4 - e.clientY;
        pos3 = e.clientX;
        pos4 = e.clientY;
        element.style.top = (element.offsetTop - pos2) + "px";
        element.style.left = (element.offsetLeft - pos1) + "px";
    }

    function closeDragElement() {
        document.onmouseup = null;
        document.onmousemove = null;
    }
}

/**
 * Moves an element in the foreground by highering its z-index value.
 *
 * @param {Object} element - The DOM object which will be moved in the foreground.
 */
function inForeground(element) {
    var panels = document.getElementsByClassName("panel");

    // Gets the currently highest z-index value of all ClickGUI panels.
    var highestZIndex = 0;
    for (var i = 0; i < panels.length; i++) {
        if (calcStyle(panels[i]).zIndex > parseInt(highestZIndex)) {
            highestZIndex = parseInt(calcStyle(panels[i]).zIndex);
        }
    }

    element.style.zIndex = `${highestZIndex + 1}`;
}

/**
 * Returns the index of the active row inside a ClickGUI panel.
 *
 * @param {Object} panel - Object of the panel for which the active row will be returned.
 * @returns {Number} - Index of the active row.
 */
function getActiveRow(panel, element) {
    // Get index of right-clicked module
    var modules = panel.getElementsByClassName("module");

    for (var i = 0; i < modules.length; i++) {
        if (modules[i].innerHTML === element.innerHTML) {
            return i;
        }
    }
}

/**
 * Function that is being called everytime the user changes a value of a module.
 *
 * @param {Object} target - Object the user interacted with.
 */
function valueChanged(target) {
    /*
        TODO: Currently unable to get the module the value belongs to. May include 'date-module' attribute with module name.
    */


    var currentModule = target.getAttribute("data-module");

    switch (target.classList[0]) {
        case "range":
            var description = target.parentElement.getElementsByClassName("description")[0];

            var title = description.innerHTML.split(" ");
            title.splice(title.length - 1, 1);

            description.innerHTML = `${title.join(" ")} ${target.value}`;

            console.log(currentModule, title.join(" "), target.value);

            window.vibrantQuery({
                "request": `set -v ${target.value} ${currentModule} ${title.join(" ")}`,
                "onSuccess": function (response) {
                },
                "onFailure": function (code, response) {
                    console.log(response);
                }
            });

            break;

        case "enumElement":
            var enumeration = target.parentElement.getElementsByClassName("enumElement");
            var title = target.parentElement.parentElement.getElementsByClassName("description")[0].innerHTML;

            for (var i = 0; i < enumeration.length; i++) {
                if (enumeration[i].classList.contains("active")) {
                    enumeration[i].classList.remove("active");
                }
            }

            target.classList.add("active");

            console.log(currentModule, title, target.innerHTML);

            window.vibrantQuery({
                "request": `set -v ${target.innerHTML} ${currentModule} ${title}`,
                "onSuccess": function (response) {
                },
                "onFailure": function (code, response) {
                    console.log(response);
                }
            });

            break;

        case "boolean":
            var title = target.innerHTML;

            if (target.classList.contains("active")) {
                target.classList.remove("active");

                console.log(currentModule, title, false);

                window.vibrantQuery({
                    "request": `set -v ${false} ${currentModule} ${title}`,
                    "onSuccess": function (response) {
                    },
                    "onFailure": function (code, response) {
                        console.log(response);
                    }
                });
            } else {
                target.classList.add("active");

                console.log(currentModule, title, true);

                window.vibrantQuery({
                    "request": `set -v ${true} ${currentModule} ${title}`,
                    "onSuccess": function (response) {
                    },
                    "onFailure": function (code, response) {
                        console.log(response);
                    }
                });
            }

            break;
    }
}

/**
 * Creates an element with all settings for a module.
 *
 * @param {Object} element - The DOM object of the module the user interacted with.
 * @param {Object} config - Object which contains information about the values the module has.
 */
function spawnOptions(element, config) {
    /* 
        TODO: Handle user scorlling.
    */

    var panel = element.parentElement.parentElement.parentElement;

    var calculatedStyle = calcStyle(document.getElementsByClassName("module")[0]);

    var moduleHeight = parseInt(calculatedStyle.height) + 2 * parseInt(calculatedStyle.padding);

    var calculatedStyle = calcStyle(element);
    var titleHeight = parseInt(calcStyle(panel.getElementsByClassName("title")[0]).height);

    var offsetLeft = parseInt(calculatedStyle.width) + 2 * parseInt(calculatedStyle.padding) + 5;
    var offsetTop = titleHeight + moduleHeight * getActiveRow(panel, element);

    var options = document.createElement("div");
    options.classList.add("options");

    // Adds options of every module to the options panel.
    Object.entries(config).forEach(
        ([key, value]) => {

            switch (value.type.toLowerCase()) {
                case "range":
                    var container = document.createElement("div");
                    container.classList.add("container");

                    var description = document.createElement("div");
                    description.classList.add("description");
                    description.innerHTML = key + " " + value.value;
                    container.appendChild(description);


                    var range = document.createElement("input");
                    range.classList.add("range");
                    range.type = "range";
                    range.min = value.min;
                    range.max = value.max;

                    if (range.min <= 1) {
                        range.step = "0.1";
                    }

                    range.value = value.value;
                    range.classList.add("range");
                    range.setAttribute("data-module", element.innerHTML);
                    container.appendChild(range);

                    range.addEventListener("change", (event) => {
                        valueChanged(event.target);
                    });

                    options.appendChild(container);

                    break;

                case "enum":
                    var container = document.createElement("div");
                    container.classList.add("container");

                    var description = document.createElement("div");
                    description.classList.add("description");
                    description.innerHTML = key;
                    container.appendChild(description);

                    var enumeration = document.createElement("div");
                    enumeration.classList.add("enum");

                    var values = value.values;

                    for (var i = 0; i < values.length; i++) {
                        var enumElement = document.createElement("div");
                        enumElement.classList.add("enumElement");
                        enumElement.setAttribute("data-module", element.innerHTML);
                        enumElement.innerHTML = values[i];

                        (function () {
                            enumElement.addEventListener("click", (event) => {
                                valueChanged(event.target);
                            });
                        }());

                        enumeration.appendChild(enumElement);
                        container.appendChild(enumeration);
                    }

                    var enumValues = container.getElementsByClassName("enumElement");

                    for (var i = 0; i < enumValues.length; i++) {
                        if (enumValues[i].innerHTML === value.value) {
                            enumValues[i].classList.add("active");
                        }
                    }

                    options.appendChild(container);

                    break;

                case "boolean":
                    var container = document.createElement("div");
                    container.classList.add("container");

                    var boolean = document.createElement("div");
                    boolean.classList.add("boolean");
                    boolean.setAttribute("data-module", element.innerHTML);
                    boolean.innerHTML = key;

                    if (value) {
                        boolean.classList.add("active");
                    }

                    boolean.addEventListener("click", (event) => {
                        valueChanged(event.target);
                    });

                    container.appendChild(boolean);
                    options.appendChild(container);

                    break;
            }
        }
    );

    options.style.top = `${offsetTop}px`;
    options.style.left = `${offsetLeft}px`;

    panel.appendChild(options);
}

/**
 * Closes an options panel.
 *
 * @param {Object} options - The DOM object of the options panel to be closed.
 */
function closeOptions(options) {
    removeElement(options);
}

window.addEventListener("mousedown", (event) => {
    try {
        inForeground(event.target.parentElement.parentElement);
    } catch (err) { }
});


window.addEventListener("click", (event) => {
    if (event.target.classList.contains("module")) {
        var clickedModule = event.target;

        if (!event.target.classList.contains("enabled")) {
            toggleModule(clickedModule, true);
        } else {
            toggleModule(clickedModule, false);
        }
    }
});

window.addEventListener("contextmenu", (event) => {
    event.preventDefault();

    if (event.target.parentElement) {
        var blur = event.target.parentElement;
        var panel = blur.parentElement.parentElement;

        var titleHeight = panel.getElementsByClassName("title")[0].clientHeight;

        if (blur.classList.contains("blur")) {
            if (blur.clientHeight > titleHeight) {
                var options = blur.parentElement.getElementsByClassName("options");

                for (var i = 0; i < options.length; i++) {
                    removeElement(options[i])
                }

                blur.style.maxHeight = `${titleHeight}px`;
            } else {
                blur.style.maxHeight = "400px"; // Get actual height
            }
        }
    }

    if (event.target.classList && event.target.classList.contains("module")) {
        var panel = event.target.parentElement.parentElement.parentElement;

        window.vibrantQuery({
            "request": "settings " + event.target.innerHTML,
            "onSuccess": function (response) {
                if (panel.getElementsByClassName("options")[0]) {
                    closeOptions(panel.getElementsByClassName("options")[0]);
                } else {
                    var config = JSON.parse(response);
                    spawnOptions(event.target, sortSettings(config));
                }
            },
            "onFailure": function (code, response) {
                console.log(response);
            }
        });
    }
});


// Initialize ClickGUI once the window finished loading.
window.addEventListener("load", () => {
    window.vibrantQuery({
        "request": "modules",
        "onSuccess": function (response) {
            var config = JSON.parse(response);

            initialize(config);

            var panels = document.getElementsByClassName("panel");

            for (var i = 0; panels.length > i; i++) {
                dragElement(panels[i]);
            }
        },
        "onFailure": function (code, response) {
            console.log(response);
        }
    });
})

const clickgui = _("#clickgui");