function _(el) {
    return document.querySelector(el);
}

const inputCommand = _("#inputCommand");
const output = _("#output");
const content = _("#content");

inputCommand.addEventListener("keydown", function (event) {
    if (event.key === "Enter") {
        event.preventDefault();

        var command = inputCommand.value;
        inputCommand.value = "";

        output.insertAdjacentHTML("beforeend", `<span class="line">&gt ${command}</span>`);

        window.vibrantQuery({
            "request": "usercommand " + command,
            "onSuccess": function (response) {
                output.insertAdjacentHTML("beforeend", `<span class="line">${response}</span>`);
                content.scrollTop = content.scrollHeight;
            },
            "onFailure": function (code, response) {
                output.insertAdjacentHTML("beforeend", `<span class="error">${response}</span>`);
                content.scrollTop = content.scrollHeight;
            }
        });
    }
});