package net.cydhra.vibrant.mixin.world;

import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.tileentity.VibrantTileEntity;
import net.cydhra.vibrant.api.world.VibrantBlockInfo;
import net.cydhra.vibrant.api.world.VibrantBlockPosition;
import net.cydhra.vibrant.api.world.VibrantChunk;
import net.cydhra.vibrant.api.world.VibrantWorld;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.entity.Entity;
import net.minecraft.profiler.Profiler;
import net.minecraft.tileentity.TileEntity;
import net.minecraft.util.BlockPos;
import net.minecraft.world.World;
import net.minecraft.world.WorldProvider;
import net.minecraft.world.storage.ISaveHandler;
import net.minecraft.world.storage.WorldInfo;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@Mixin(WorldClient.class)
public abstract class WorldMixin extends World implements VibrantWorld {

    protected WorldMixin(ISaveHandler iSaveHandler, WorldInfo worldInfo, WorldProvider worldProvider, Profiler profiler, boolean isRemote) {
        super(iSaveHandler, worldInfo, worldProvider, profiler, isRemote);
    }

    @NotNull
    @Override
    public List<VibrantEntity> getEntityList() {
        return this.loadedEntityList.stream().map(
                entity -> (VibrantEntity) entity
        ).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public List<VibrantTileEntity> getTileEntityList() {
        return this.loadedTileEntityList.stream().map(
                entity -> (VibrantTileEntity) entity
        ).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public VibrantChunk getChunkFromBlockCoordinates(@NotNull VibrantBlockPosition position) {
        return (VibrantChunk) this.getChunkFromBlockCoords((BlockPos) position);
    }

    @NotNull
    @Override
    public VibrantBlockInfo getBlockInfoAt(@NotNull VibrantBlockPosition position) {
        return (VibrantBlockInfo) this.getBlockState((net.minecraft.util.BlockPos) position).getBlock();
    }
}
