package net.cydhra.vibrant.mixin.world;

import net.cydhra.vibrant.api.world.VibrantBlockFacing;
import net.cydhra.vibrant.api.world.VibrantBlockPosition;
import net.minecraft.util.BlockPos;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3i;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(BlockPos.class)
public abstract class BlockPosMixin extends Vec3i implements VibrantBlockPosition {

    public BlockPosMixin(int x, int y, int z) {
        super(x, y, z);
    }

    public BlockPosMixin(double x, double y, double z) {
        super(x, y, z);
    }

    @Shadow
    public abstract BlockPos add(int x, int y, int z);

    @Shadow
    public abstract BlockPos add(double x, double y, double z);

    @Shadow
    public abstract BlockPos offset(EnumFacing facing);

    @Shadow
    public abstract BlockPos offset(EnumFacing facing, int count);

    @Shadow
    public abstract BlockPos up();

    @Shadow
    public abstract BlockPos up(int count);

    @Shadow
    public abstract BlockPos down();

    @Shadow
    public abstract BlockPos down(int count);

    @Shadow
    public abstract BlockPos south();

    @Shadow
    public abstract BlockPos south(int count);

    @Shadow
    public abstract BlockPos north();

    @Shadow
    public abstract BlockPos north(int count);

    @Shadow
    public abstract BlockPos west();

    @Shadow
    public abstract BlockPos west(int count);

    @Shadow
    public abstract BlockPos east();

    @Shadow
    public abstract BlockPos east(int count);

    @Override
    public int getPosX() {
        return this.getX();
    }

    @Override
    public int getPosY() {
        return this.getY();
    }

    @Override
    public int getPosZ() {
        return this.getZ();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetSide(@NotNull VibrantBlockFacing facing) {
        return (VibrantBlockPosition) this.offset(EnumFacing.values()[facing.ordinal()]);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetSide(@NotNull VibrantBlockFacing facing, int count) {
        return (VibrantBlockPosition) this.offset(EnumFacing.values()[facing.ordinal()], count);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetBy(double x, double y, double z) {
        return (VibrantBlockPosition) this.add(x, y, z);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetBy(int x, int y, int z) {
        return (VibrantBlockPosition) this.add(x, y, z);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetUp() {
        return (VibrantBlockPosition) this.up();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetUp(int count) {
        return (VibrantBlockPosition) this.up(count);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetDown() {
        return (VibrantBlockPosition) this.down();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetDown(int count) {
        return (VibrantBlockPosition) this.down(count);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetNorth() {
        return (VibrantBlockPosition) this.north();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetNorth(int count) {
        return (VibrantBlockPosition) this.north(count);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetEast() {
        return (VibrantBlockPosition) this.east();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetEast(int count) {
        return (VibrantBlockPosition) this.east(count);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetSouth() {
        return (VibrantBlockPosition) this.south();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetSouth(int count) {
        return (VibrantBlockPosition) this.south(count);
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetWest() {
        return (VibrantBlockPosition) this.west();
    }

    @NotNull
    @Override
    public VibrantBlockPosition offsetWest(int count) {
        return (VibrantBlockPosition) this.west(count);
    }
}
