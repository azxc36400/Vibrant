package net.cydhra.vibrant.mixin.item;

import net.cydhra.vibrant.api.item.VibrantArmorMaterial;
import net.cydhra.vibrant.api.item.VibrantArmorType;
import net.cydhra.vibrant.api.item.VibrantItemArmor;
import net.minecraft.item.ItemArmor;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(ItemArmor.class)
public abstract class ItemArmorMixin implements VibrantItemArmor {

    @Shadow
    public ItemArmor.ArmorMaterial material;

    @Shadow
    public int armorType;

    @Override
    public VibrantArmorMaterial getMaterial() {
        switch (this.material) {
            case LEATHER:
                return VibrantArmorMaterial.LEATHER;
            case CHAIN:
                return VibrantArmorMaterial.CHAIN;
            case IRON:
                return VibrantArmorMaterial.IRON;
            case GOLD:
                return VibrantArmorMaterial.GOLD;
            case DIAMOND:
                return VibrantArmorMaterial.DIAMOND;
            default:
                throw new AssertionError();
        }
    }

    @Override
    public VibrantArmorType getType() {
        switch (this.armorType) {
            case 3:
                return VibrantArmorType.BOOTS;
            case 2:
                return VibrantArmorType.LEGS;
            case 1:
                return VibrantArmorType.CHEST;
            case 0:
                return VibrantArmorType.HEAD;
            default:
                throw new AssertionError();
        }
    }
}
