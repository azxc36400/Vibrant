package net.cydhra.vibrant.mixin.entity;

import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.util.VibrantBoundingBox;
import net.cydhra.vibrant.api.util.VibrantVec3;
import net.cydhra.vibrant.api.world.VibrantBlockFacing;
import net.minecraft.entity.Entity;
import net.minecraft.util.AxisAlignedBB;
import net.minecraft.util.EnumFacing;
import net.minecraft.util.Vec3;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Entity.class)
public abstract class EntityMixin implements VibrantEntity {

    @Shadow
    public double prevPosX;

    @Shadow
    public double prevPosY;

    @Shadow
    public double prevPosZ;

    @Shadow
    public double posX;

    @Shadow
    public double posY;

    @Shadow
    public double posZ;

    @Shadow
    public double motionX;

    @Shadow
    public double motionY;

    @Shadow
    public double motionZ;

    @Shadow
    public float rotationYaw;

    @Shadow
    public float rotationPitch;

    @Shadow
    public float prevRotationYaw;

    @Shadow
    public float prevRotationPitch;

    @Shadow
    public boolean onGround;

    @Shadow
    public float fallDistance;

    @Shadow
    public float width;

    @Shadow
    public float height;

    @Shadow
    public float stepHeight;

    @Shadow
    protected EnumFacing field_181018_ap;

    @Shadow
    public boolean isCollidedHorizontally;

    @Shadow
    public boolean isCollidedVertically;

    @Shadow
    public abstract EnumFacing getHorizontalFacing();

    @Shadow
    protected abstract void setRotation(float yaw, float pitch);

    @Shadow
    public abstract double getDistanceSq(double x, double y, double z);

    @Shadow
    public abstract AxisAlignedBB getEntityBoundingBox();

    @Shadow
    public abstract String getName();

    @Shadow
    public abstract void setPosition(double x, double y, double z);

    @Shadow
    public abstract void setPositionAndRotation(double x, double y, double z, float yaw, float pitch);

    @Shadow
    public abstract void setLocationAndAngles(double x, double y, double z, float yaw, float pitch);

    @Shadow
    public abstract Vec3 getPositionVector();

    @Shadow
    public abstract float getEyeHeight();

    @Shadow
    public abstract boolean isSprinting();

    @Shadow
    public abstract void setSprinting(boolean sprinting);

    @Override
    public double getPrevPosX() {
        return this.prevPosX;
    }

    @Override
    public double getPrevPosY() {
        return this.prevPosY;
    }

    @Override
    public double getPrevPosZ() {
        return this.prevPosZ;
    }

    @Override
    public double getMotionX() {
        return this.motionX;
    }

    @Override
    public void setMotionX(double motionX) {
        this.motionX = motionX;
    }

    @Override
    public double getMotionY() {
        return this.motionY;
    }

    @Override
    public void setMotionY(double motionY) {
        this.motionY = motionY;
    }

    @Override
    public double getMotionZ() {
        return this.motionZ;
    }

    @Override
    public void setMotionZ(double motionZ) {
        this.motionZ = motionZ;
    }

    @Override
    public float getRotationYaw() {
        return this.rotationYaw;
    }

    @Override
    public float getRotationPitch() {
        return this.rotationPitch;
    }

    @Override
    public boolean getOnGround() {
        return this.onGround;
    }

    @Override
    public void setOnGround(boolean onGround) {
        this.onGround = onGround;
    }

    @Override
    public float getFallDistance() {
        return this.fallDistance;
    }

    @Override
    public void setFallDistance(float fallDistance) {
        this.fallDistance = fallDistance;
    }

    @Override
    public float getWidth() {
        return this.width;
    }

    @Override
    public void setWidth(float width) {
        this.width = width;
    }

    @Override
    public float getHeight() {
        return this.height;
    }

    @Override
    public void setHeight(float height) {
        this.height = height;
    }

    @Override
    public void setStepHeight(float stepHeight) {
        this.stepHeight = stepHeight;
    }

    @Override
    public float getStepHeight() {
        return this.stepHeight;
    }

    @NotNull
    @Override
    public VibrantBlockFacing getFacing() {
        return VibrantBlockFacing.valueOf(this.field_181018_ap.name());
    }

    @NotNull
    @Override
    public VibrantBlockFacing getHorizontalBlockFacing() {
        return VibrantBlockFacing.valueOf(this.getHorizontalFacing().name());
    }

    @Override
    public void setEntityRotation(float yaw, float pitch) {
        this.setRotation(yaw, pitch);
    }

    @Override
    public boolean isCollidedHorizontally() {
        return this.isCollidedHorizontally;
    }

    @Override
    public boolean isCollidedVertically() {
        return this.isCollidedVertically;
    }

    @Override
    public double getPosX() {
        return this.posX;
    }

    @Override
    public double getPosY() {
        return this.posY;
    }

    @Override
    public double getPosZ() {
        return this.posZ;
    }

    @NotNull
    @Override
    public VibrantBoundingBox getBoundingBox() {
        return (VibrantBoundingBox) this.getEntityBoundingBox();
    }

    @Override
    public double getDistanceSquared(double x, double y, double z) {
        return this.getDistanceSq(x, y, z);
    }

    @NotNull
    @Override
    public String getEntityName() {
        return this.getName();
    }

    @Override
    public void setEntityPosition(double posX, double posY, double posZ) {
        this.setPosition(posX, posY, posZ);
    }

    @Override
    public void setEntityPositionAndRotation(double posX, double posY, double posZ, float yaw, float pitch) {
        this.setPositionAndRotation(posX, posY, posZ, yaw, pitch);
    }

    @Override
    public void setEntityLocationAndAngles(double posX, double posY, double posZ, float yaw, float pitch) {
        this.setLocationAndAngles(posX, posY, posZ, yaw, pitch);
    }

    @NotNull
    @Override
    public VibrantVec3 getLocationVector() {
        return (VibrantVec3) this.getPositionVector();
    }

    @Override
    public float getEntityEyeHeight() {
        return this.getEyeHeight();
    }

    @Override
    public boolean isEntitySprinting() {
        return this.isSprinting();
    }

    @Override
    public void setEntitySprinting(boolean sprinting) {
        this.setSprinting(sprinting);
    }
}
