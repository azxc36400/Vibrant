package net.cydhra.vibrant.mixin.inventory;

import net.cydhra.vibrant.api.entity.VibrantPlayer;
import net.cydhra.vibrant.api.inventory.VibrantContainer;
import net.cydhra.vibrant.api.inventory.VibrantPlayerInventory;
import net.cydhra.vibrant.api.item.VibrantItemStack;
import net.cydhra.vibrant.api.network.ClickType;
import net.minecraft.entity.player.EntityPlayer;
import net.minecraft.entity.player.InventoryPlayer;
import net.minecraft.inventory.Container;
import net.minecraft.item.ItemStack;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Container.class)
public abstract class ContainerMixin implements VibrantContainer {

    @Shadow
    private int windowId;

    @Shadow
    public abstract short getNextTransactionID(InventoryPlayer p_75136_1_);

    @Shadow
    public abstract ItemStack slotClick(int slotId, int clickedButton, int mode, EntityPlayer playerIn);

    @Override
    public int getWindowId() {
        return this.windowId;
    }

    @Override
    public short nextTransactionID(@NotNull VibrantPlayerInventory inventory) {
        return this.getNextTransactionID((net.minecraft.entity.player.InventoryPlayer) inventory);
    }

    @NotNull
    @Override
    public VibrantItemStack clickSlot(int slotId, int clickedButton, @NotNull ClickType mode, @NotNull VibrantPlayer player) {
        return VibrantItemStack.class.cast(this.slotClick(slotId, clickedButton, mode.ordinal(), (EntityPlayer) player));
    }
}
