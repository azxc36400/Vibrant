package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.render.VibrantFrustum;
import net.cydhra.vibrant.api.util.VibrantBoundingBox;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.util.AxisAlignedBB;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(Frustum.class)
public abstract class FrustumMixin implements VibrantFrustum {

    @Shadow
    public abstract boolean isBoundingBoxInFrustum(AxisAlignedBB boundingBox);

    @Override
    public boolean isBoundingBoxInsideFrustum(@NotNull VibrantBoundingBox boundingBox) {
        return this.isBoundingBoxInFrustum((AxisAlignedBB) boundingBox);
    }
}
