package net.cydhra.vibrant.mixin.hook;

import net.minecraft.client.Minecraft;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Minecraft.class)
public abstract class GuiFpsPatchMixin {

    @Inject(method = "getLimitFramerate", at = @At("HEAD"), cancellable = true)
    protected void getLimitFramerate(final CallbackInfoReturnable<Integer> info) {
        if (Minecraft.getMinecraft().currentScreen != null) {
            info.setReturnValue(60);
            info.cancel();
        }
    }
}
