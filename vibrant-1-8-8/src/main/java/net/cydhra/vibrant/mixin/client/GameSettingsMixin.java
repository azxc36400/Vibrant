package net.cydhra.vibrant.mixin.client;

import net.cydhra.vibrant.api.client.VibrantGameSettings;
import net.minecraft.client.settings.GameSettings;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(GameSettings.class)
public abstract class GameSettingsMixin implements VibrantGameSettings {

    @Shadow
    public boolean entityShadows;

    @Shadow
    public int guiScale;

    @Shadow
    public float gammaSetting;

    @Override
    public boolean getRenderEntityShadows() {
        return this.entityShadows;
    }

    @Override
    public void setRenderEntityShadows(final boolean renderEntityShadows) {
        this.entityShadows = renderEntityShadows;
    }

    @Override
    public int getGuiScale() {
        return this.guiScale;
    }

    @Override
    public void setGuiScale(final int guiScale) {
        this.guiScale = guiScale;
    }

    @Override
    public float getGammaSetting() {
        return this.gammaSetting;
    }

    @Override
    public void setGammaSetting(final float gammaSetting) {
        this.gammaSetting = gammaSetting;
    }
}
