package net.cydhra.vibrant.mixin.render;

import net.cydhra.vibrant.api.entity.VibrantEntity;
import net.cydhra.vibrant.api.render.VibrantFrustum;
import net.cydhra.vibrant.api.render.VibrantRenderGlobal;
import net.minecraft.client.renderer.RenderGlobal;
import net.minecraft.client.renderer.culling.Frustum;
import net.minecraft.client.renderer.culling.ICamera;
import net.minecraft.entity.Entity;
import net.minecraft.util.EnumWorldBlockLayer;
import org.jetbrains.annotations.NotNull;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;

@Mixin(RenderGlobal.class)
public abstract class RenderGlobalMixin implements VibrantRenderGlobal {

    @Shadow
    public abstract void setupTerrain(Entity entity, double partialTicks, ICamera camera, int frameCount, boolean b);

    @Shadow
    public abstract int renderBlockLayer(EnumWorldBlockLayer layer, double p_renderBlockLayer_2_, int p_renderBlockLayer_4_, Entity entity);

    @Override
    public void setupTerrain(@NotNull VibrantEntity viewEntity, float partialTicks, @NotNull VibrantFrustum frustum, int frameCount) {
        this.setupTerrain((Entity) viewEntity, partialTicks, (Frustum) frustum, frameCount, false);
    }

    @Override
    public void renderBlockLayer(@NotNull VibrantBlockLayerType type, @NotNull VibrantEntity viewEntity) {
        EnumWorldBlockLayer layerType = null;

        switch (type) {
            case SOLID:
                layerType = EnumWorldBlockLayer.SOLID;
                break;
            case TRANSLUCENT:
                layerType = EnumWorldBlockLayer.TRANSLUCENT;
                break;
            case CUTOUT:
                layerType = EnumWorldBlockLayer.CUTOUT;
                break;
            case CUTOUT_MIPPED:
                layerType = EnumWorldBlockLayer.CUTOUT_MIPPED;
                break;
        }

        this.renderBlockLayer(layerType, 0, 0, (Entity) viewEntity);
    }
}
